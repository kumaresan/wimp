package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class EBayTransaction extends Model {

	public String transactionid;
	public String itemid;
	public String title;
	public String url;
	public String imageUrl;

	@ManyToOne
	public EBayUser seller;

	@ManyToOne
	public EBayUser buyer;

	public String trackingid;
	public String carrier;

	public String sellerAddress;
	public String sellerLat;
	public String sellerLng;

	public String buyerAddress;
	public String buyerLat;
	public String buyerLng;

	public Boolean delivered;

	public EBayTransaction(String transactionid, String itemid, String title,
			String url, String imageUrl, EBayUser seller, EBayUser buyer,
			String trackingid, String carrier, String sellerAddress,
			String sellerLat, String sellerLng, String buyerAddress,
			String buyerLat, String buyerLng, Boolean delivered) {
		super();
		this.transactionid = transactionid;
		this.itemid = itemid;
		this.title = title;
		this.url = url;
		this.imageUrl = imageUrl;
		this.seller = seller;
		this.buyer = buyer;
		this.trackingid = trackingid;
		this.carrier = carrier;
		this.sellerAddress = sellerAddress;
		this.sellerLat = sellerLat;
		this.sellerLng = sellerLng;
		this.buyerAddress = buyerAddress;
		this.buyerLat = buyerLat;
		this.buyerLng = buyerLng;
		this.delivered = delivered;
	}

	@Override
	public String toString() {
		return "<" + buyer + "> bought <" + title + "(" + itemid + ")> from <" + seller + ">";
	}
}
