package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class EBayItem extends Model {
	public String itemid;
	public String title;
	public String url;
	public String imageUrl;

	public EBayItem(String itemid, String title, String url, String imageUrl) {
		super();
		this.itemid = itemid;
		this.title = title;
		this.url = url;
		this.imageUrl = imageUrl;
	}
	@Override
	public String toString() {
		return itemid;
	}

}
