package models;

import java.util.List;

public class WimpModelManager {

	public static void addUser(String userid, String authtoken) {
		EBayUser eBayUser = new EBayUser(userid, authtoken, null, null);
		eBayUser.save();
	}

	public static EBayUser getUser(String userid) throws Exception {
		EBayUser eBayUser = EBayUser.find("byUserid", userid).first();
		if (eBayUser == null) {
			throw new Exception("User Not Found " + userid);
		}
		return eBayUser;
	}

	public static void addPhoneToUser(String userid, String phoneno)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		eBayUser.phoneno = phoneno;
		eBayUser.save();
	}

	public static void addUserItem(String itemid, String title, String url,
			String imageUrl) throws Exception {
		EBayItem eBayItem = new EBayItem(itemid, title, url, imageUrl);
		eBayItem.save();
	}

	public static List<EBayTransaction> getBuyerTrasactions(String userid)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		List<EBayTransaction> transactions = EBayTransaction.find("byBuyer",
				eBayUser).fetch();
		return transactions;
	}

	public static List<EBayTransaction> getSellerTransactions(String userid)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		List<EBayTransaction> transactions = EBayTransaction.find("bySeller",
				eBayUser).fetch();
		return transactions;
	}

}
