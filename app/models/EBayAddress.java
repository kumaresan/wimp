package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class EBayAddress extends Model {

	@ManyToOne
	public EBayTransaction transaction;

	public Integer sequence;

	public String address;
	public String scandiscription;

	public Boolean alerted;

	public String lat;
	public String lng;

	@Override
	public String toString() {
		return address + " :: " + transaction;
	}
}
