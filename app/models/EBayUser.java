package models;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class EBayUser extends Model {
	public String userid;
	public String authToken;
	public String phoneno;
	public String email;

	public EBayUser(String userid, String authToken, String phoneno,
			String email) {
		super();
		this.userid = userid;
		this.authToken = authToken;
		this.phoneno = phoneno;
		this.email = email;
	}

	@Override
	public String toString() {
		return userid;
	}

}
