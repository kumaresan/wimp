//package ebay;
//
//import java.io.BufferedReader;
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.PrintWriter;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;
//
//import wimp.AddressUtil;
//
//public class EbayXmlApi {
//
//	public static String DEV_ID = "2b6cc293-a846-4a4c-99d0-91a890ab0d12";
//	public static String APP_ID = "Property-1900-451f-80e7-95938e66a123";
//	public static String CERT_ID = "0e73b624-3db3-4914-8d36-2b750a03ea17";
//	public static String RU_NAME = "Property_Bees-Property-1900-4-ycemmil";
//	public static String SERVER_URL = "https://api.sandbox.ebay.com/wsapi";
//	public static String SIGNIN_URL = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn";
//
//	public static String endpoint = "https://api.sandbox.ebay.com/ws/api.dll";
//
//	public static URLConnection getConnection(String callName) throws Exception {
//		URL url = new URL(endpoint);
//		URLConnection connection = null;
//		connection = url.openConnection();
//		connection.setUseCaches(false);
//		connection.setDoOutput(true);
//		connection
//				.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
//		connection.setRequestProperty("X-EBAY-API-COMPATIBILITY-LEVEL", "759");
//		connection.setRequestProperty("X-EBAY-API-SITEID", "0");
//
//		connection.setRequestProperty("X-EBAY-API-CALL-NAME", callName);
//		connection.setRequestProperty("X-EBAY-API-DEV-NAME", DEV_ID);
//		connection.setRequestProperty("X-EBAY-API-APP-NAME", APP_ID);
//		connection.setRequestProperty("X-EBAY-API-CERT-NAME", CERT_ID);
//
//		return connection;
//	}
//
//	public static String callEbay(String callName, String request)
//			throws Exception {
//		System.out.println("EbayXmlApi.callEbay() callName " + callName);
//		System.out.println("EbayXmlApi.callEbay() request " + request);
//		URLConnection connection = getConnection(callName);
//		PrintWriter pw = new PrintWriter(connection.getOutputStream());
//		pw.write(request);
//		pw.flush();
//		pw.close();
//		BufferedReader inBuff = new BufferedReader(new InputStreamReader(
//				connection.getInputStream()));
//		String decodedString;
//		String response = "";
//		while ((decodedString = inBuff.readLine()) != null) {
//			response += decodedString;
//		}
//		System.out.println("EbayXmlApi.callEbay() response " + response);
//		return response;
//	}
//
//	public static String getValueFromXml(String response, String field) {
//		System.out.println("EbayXmlApi.getValueFromXml() response " + response);
//		System.out.println("EbayXmlApi.getValueFromXml() field " + field);
//
//		if (response == null) {
//			return null;
//		}
//
//		String[] temp = response.split("<" + field + ">");
//
//		if (temp.length != 2) {
//			return null;
//		}
//
//		temp = temp[1].split("</" + field + ">");
//
//		if (temp.length != 2) {
//			return null;
//		}
//
//		System.out.println("EbayXmlApi.getValueFromXml() Value " + temp[0]);
//
//		return temp[0];
//	}
//
//	public static String geteBayTime(String authToken) throws Exception {
//		String request = "<?xml version='1.0' encoding='utf-8'?>"
//				+ "<GeteBayOfficialTimeRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"
//				+ "  <RequesterCredentials>" + "<eBayAuthToken>" + authToken
//				+ "</eBayAuthToken>" + "  </RequesterCredentials>"
//				+ "</GeteBayOfficialTimeRequest>";
//		String response = callEbay("GeteBayOfficialTime", request);
//		String time = getValueFromXml(response, "Timestamp");
//		return time;
//	}
//
//	public static String getSessionID() throws Exception {
//		String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//				+ "<GetSessionIDRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"
//				+ "<RuName>" + RU_NAME + "</RuName>" + "</GetSessionIDRequest>";
//
//		String response = callEbay("GetSessionID", request);
//
//		String sessionId = getValueFromXml(response, "SessionID");
//
//		return sessionId;
//
//	}
//
//	public static String getSignInUrl(String sessionID) throws Exception {
//		String ruParams = "params=" + RU_NAME + "-Sandbox";
//
//		String signInUrl = SIGNIN_URL + "&RuName=" + RU_NAME + "&SessID="
//				+ sessionID + "&ruparams=" + ruParams;
//
//		System.out.println("EbayXmlApi.getSigninUrl() signInUrl " + signInUrl);
//
//		return signInUrl;
//	}
//
//	public static String fetchToken(String sessionID) throws Exception {
//		String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//				+ "<FetchTokenRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"
//				+ "<SessionID>" + sessionID + "</SessionID>"
//				+ "</FetchTokenRequest>";
//
//		String response = callEbay("FetchToken", request);
//
//		String authToken = getValueFromXml(response, "eBayAuthToken");
//
//		return authToken;
//	}
//
//	public static String getUserID(String authToken) throws Exception {
//		String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//				+ "<GetUserRequest  xmlns=\"urn:ebay:apis:eBLBaseComponents\">"
//				+ "<RequesterCredentials><eBayAuthToken>" + authToken
//				+ "</eBayAuthToken></RequesterCredentials>"
//				+ "</GetUserRequest>";
//
//		String response = callEbay("GetUser", request);
//
//		String userid = getValueFromXml(response, "UserID");
//
//		return userid;
//	}
//
//	public static List<String> getBoughtItems(String authToken)
//			throws Exception {
//		StringBuilder bos = new StringBuilder();
//		bos.append("<?xml version=\"1.0\" encoding=\"utf-8\"?> <GetMyeBayBuyingRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\"> <Version>759</Version> <DetailLevel>ReturnAll</DetailLevel> <OutputSelector>ItemId</OutputSelector><RequesterCredentials>");
//		bos.append("<eBayAuthToken>");
//		bos.append(authToken);
//		bos.append("</eBayAuthToken>");
//		bos.append("</RequesterCredentials>");
//		bos.append("</GetMyeBayBuyingRequest>");
//		String request = bos.toString();
//
//		String response = callEbay("GetMyeBayBuying", request);
//
//		return parseXMLForScans(response, "Item", "ItemID");
//
//	}
//
//	public static String getTrackingDetails(String authToken, String itemid)
//			throws Exception {
//		StringBuilder bos = new StringBuilder();
//		bos.append("<?xml version=\"1.0\" encoding=\"utf-8\"?> <GetItemTransactionsRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\"> <Version>759</Version> <DetailLevel>ReturnAll</DetailLevel>");
//		bos.append("<ItemID>");
//		bos.append(itemid);
//		bos.append("</ItemID>");
//		bos.append("<RequesterCredentials>");
//		bos.append("<eBayAuthToken>");
//		bos.append(authToken);
//		bos.append("</eBayAuthToken>");
//		bos.append("</RequesterCredentials>");
//		bos.append("</GetItemTransactionsRequest>");
//		String request = bos.toString();
//
//		String response = callEbay("GetItemTransactions", request);
//
//		// return parseXMLForScans(response, "Item", "ItemID");
//
//		return response;
//
//	}
//
//	public static String getItemDetails(String authToken, String itemid)
//			throws Exception {
//		StringBuilder bos = new StringBuilder();
//		bos.append("<?xml version=\"1.0\" encoding=\"utf-8\"?> <GetItemShippingRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\"> <Version>759</Version> <DetailLevel>ReturnAll</DetailLevel>");
//		bos.append("<ItemID>");
//		bos.append(itemid);
//		bos.append("</ItemID>");
//		bos.append("<DestinationPostalCode>22313</DestinationPostalCode> <DestinationCountryCode>US</DestinationCountryCode>");
//		bos.append("<RequesterCredentials>");
//		bos.append("<eBayAuthToken>");
//		bos.append(authToken);
//		bos.append("</eBayAuthToken>");
//		bos.append("</RequesterCredentials>");
//		bos.append("</GetItemShippingRequest>");
//		String request = bos.toString();
//
//		String response = callEbay("GetItemShipping", request);
//
//		// return parseXMLForScans(response, "Item", "ItemID");
//
//		return response;
//
//	}
//
//	private static List<String> parseXMLForScans(String response,
//			String parent, String field) throws ParserConfigurationException,
//			SAXException, IOException {
//		List<String> itemIds = new ArrayList<String>();
//
//		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//		DocumentBuilder db = dbf.newDocumentBuilder();
//		Document scanDetails = db.parse(new ByteArrayInputStream(response
//				.getBytes()));
//
//		NodeList scanData = scanDetails.getElementsByTagName(parent);
//
//		for (int temp = 0; temp < scanData.getLength(); temp++) {
//
//			Node nNode = scanData.item(temp);
//			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//				Element eElement = (Element) nNode;
//
//				itemIds.add(getTagValue(field, eElement));
//			}
//		}
//		return itemIds;
//	}
//
//	public static List<AddressUtil> getLocations() throws Exception {
//
//		List<String> trackerInfo = new ArrayList<String>();
//		trackerInfo = getTrackerForItems();
//
//		TrackingDetails trackingdetailobj = new TrackingDetails();
//		List<AddressUtil> addObj = trackingdetailobj.trackerDetails(
//				trackerInfo.get(0), trackerInfo.get(1));
//
//		return addObj;
//	}
//
//	private static String getTagValue(String sTag, Element eElement) {
//		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
//				.getChildNodes();
//
//		Node nValue = (Node) nlList.item(0);
//		if (nValue != null)
//			return nValue.getNodeValue();
//		else
//			return "Not Found !!";
//	}
//
//	private static List<String> getTrackerForItems() throws Exception {
//
//		String trackerid = "RF074932865SG";
//		String carriername = "4PX";
//
//		List<String> details = new ArrayList<String>();
//		details.add(trackerid);
//		details.add(carriername);
//
//		return details;
//	}
//}
