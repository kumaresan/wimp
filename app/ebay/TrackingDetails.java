//package ebay;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.PrintWriter;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;
//
//public class TrackingDetails {
//	// public static String addressString = "";
//	// public static String finalLocation = "";
//
//	public static void main(String[] args) throws Exception {
//		trackerDetails("RF074932865SG", "4PX");
//	}
//
//	public static List<AddressUtil> trackerDetails(String tracker_id, String carrier_name) {
//
//		StringBuilder bos = new StringBuilder();
//		bos.append("<?xml version=\"1.0\"  encoding=\"utf-16\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cbt=\"http://cbtagg.ebay.cn/CBTAgg/\"><soapenv:Header><cbt:RequestHeader><cbt:PartnerID>ebayus</cbt:PartnerID><cbt:Password>ebayus</cbt:Password><cbt:PartnerTransactionID>1234</cbt:PartnerTransactionID><cbt:WSVersionNumber>TH_V0910</cbt:WSVersionNumber><cbt:PartnerTransactionDate>2011-11-15 05:14:10</cbt:PartnerTransactionDate></cbt:RequestHeader></soapenv:Header><soapenv:Body><cbt:TrackPackage><cbt:TrackingRequestDataList><cbt:TrackingRequestData> ");
//		bos.append("<cbt:ShipDateRangeEnd></cbt:ShipDateRangeEnd><cbt:PackageIdentifierType>TrackingNumber</cbt:PackageIdentifierType><cbt:DestinationCountryCode>US</cbt:DestinationCountryCode><cbt:ShipDateRangeBegin></cbt:ShipDateRangeBegin><cbt:AccountNumber></cbt:AccountNumber><cbt:ReportScans>SummaryAndDetails</cbt:ReportScans><cbt:CarrierName>");
//		bos.append(carrier_name);
//		bos.append("</cbt:CarrierName><cbt:TrackingNumber>");
//		bos.append(tracker_id);
//		bos.append("</cbt:TrackingNumber><cbt:ItemLocationCountry>US</cbt:ItemLocationCountry></cbt:TrackingRequestData> </cbt:TrackingRequestDataList></cbt:TrackPackage></soapenv:Body></soapenv:Envelope>");
//
//		List<AddressUtil> addressString = new ArrayList<AddressUtil>();
//		String req = bos.toString();
//		URL url = null;
//		try {
//			url = new URL(
//					"http://globalshippingtracking.ebay.com/shippinghub/services/CBTAgg");
//			URLConnection connection = null;
//			connection = url.openConnection();
//			connection.setUseCaches(false);
//			connection.setDoOutput(true);
//			connection.setRequestProperty("accept-charset", "UTF-8");
//			connection.setRequestProperty("content-type",
//					"application/x-www-form-urlencoded");
//			PrintWriter pw = new PrintWriter(connection.getOutputStream());
//			pw.write(req);
//			pw.flush();
//			pw.close();
//			BufferedReader inBuff = new BufferedReader(new InputStreamReader(
//					connection.getInputStream()));
//			String decodedString;
//			String response = "";
//			while ((decodedString = inBuff.readLine()) != null) {
//				response += decodedString;
//			}
//			FileWriter fstreamwrite = new FileWriter("response.xml");
//			BufferedWriter out = new BufferedWriter(fstreamwrite);
//			out.write(response);
//			out.close();
//			System.out.println("response is : " + response);
//			addressString = parseXMLForScans("response.xml");
//			inBuff.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		int index = 0;
//		
//		for ( AddressUtil add : addressString)
//		{
//			System.out.println(add.getCity());
//		}
//		
//		for ( AddressUtil add : addressString) {
//			if (add.getCity().isEmpty()) {
//				if ( index == 0 )
//				{
//					System.out.println("Shipping details not available");
//					break;
//				}
//				add.setCity(addressString.get(index - 1).getCity());
//			}
//			index ++;
//		}
//		
//		for ( AddressUtil add : addressString)
//		{
//			System.out.println(add.getCity());
//		}
//		
//		return addressString;
////		System.out.println("address string is : " + addressString.get(0).getCity());
//	}
//
//	private static List<AddressUtil> parseXMLForScans(String fileName)
//			throws ParserConfigurationException, SAXException, IOException {
//		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//		DocumentBuilder db = dbf.newDocumentBuilder();
//		Document scanDetails = db.parse(fileName);
//		NodeList scanData = scanDetails.getElementsByTagName("ns2:ScanDetails");
//		List<AddressUtil> addressString = new ArrayList<AddressUtil>();
//		for (int temp = 0; temp < scanData.getLength(); temp++) {
//
//			Node nNode = scanData.item(temp);
//			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//				Element eElement = (Element) nNode;
//
//				addressString
//						.add(new AddressUtil(getTagValue("ns2:EventCity", eElement),
//								getTagValue("ns2:ScanDescription", eElement)));
//			}
//		}
//		return addressString;
//	}
//
//	private static String getTagValue(String sTag, Element eElement) {
//		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
//				.getChildNodes();
//
//		Node nValue = (Node) nlList.item(0);
//		if (nValue != null)
//			return nValue.getNodeValue();
//		else
//			return "";
//	}
//
//}
