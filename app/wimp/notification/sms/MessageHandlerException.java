package wimp.notification.sms;

public class MessageHandlerException extends Exception
{
  private static final long serialVersionUID = 164042307668799943L;
  public MessageHandlerException.ErrorCode code;

  public MessageHandlerException(MessageHandlerException.ErrorCode code)
  {
    this.code = code;
  }

  public static enum ErrorCode
  {
    INTERNAL_FAILURE, 
    CONNECTION_ERROR, 
    BACKEND_ERROR, 
    LOGIN_FAILURE, 
    COOKIE_EXPIRED;
  }
}