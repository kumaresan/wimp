package wimp.notification.sms;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class MessageSender
{
  private static final String AUTH_URL = "http://wwwe.way2sms.com/Login1.action";
  private static final String MSG_URL = "http://wwwe.way2sms.com/quicksms.action";
  private static final String USER_ID_URL = "http://wwwe.way2sms.com/jsp/InstantSMS.jsp";
  private static final String LOGIN_STATIC_POST_DATA = "button=Login&";
  private static final String MSG_STATIC_POST_DATA = "HiddenAction=instantsms&";
  private static final String LOGIN_REFERRER = "http://wwwe.way2sms.com/content/index.html";
  private static final String MSG_REFERRER = "http://wwwe.way2sms.com/jsp/InstantSMS.jsp";
  private static final String LOGIN_SUCCESS_LOCATION = "Main.action";
  private static final char[] USER_ID_NODE_MATCHER = { 'd', '=', '"', 'A', 'c', 't', 'i', 'o', 'n', '"' };
  private static Map<String, String> MAP = new HashMap(5);
  private static int usedIdNodeMatcherCounter;
  private static StringBuffer userIdMatcher;
  private static boolean extractionStarted;

  private static String encode(String data)
  {
    try
    {
      return URLEncoder.encode(data, "UTF-8");
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {
    }
    return "";
  }

  private static void setupConnection(HttpURLConnection conn) {
    conn.setDoInput(true);
    conn.setDoOutput(true);
    conn.setUseCaches(false);
    conn.setAllowUserInteraction(false);
    conn.setInstanceFollowRedirects(false);
    conn.setRequestProperty("Host", "wwwe.way2sms.com");
    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13");
    conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    conn.setRequestProperty("Accept-Language", "en-us,en;q=0.5");
    conn.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
    conn.setRequestProperty("Keep-Alive", "115");
    conn.setRequestProperty("Connection", "keep-alive");
  }

  private static String login(String userId, String password) throws IOException, MessageHandlerException {
    URL url = new URL("http://wwwe.way2sms.com/Login1.action");
    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    setupConnection(conn);
    conn.setRequestProperty("Referer", "http://wwwe.way2sms.com/content/index.html");
    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    String postData = "button=Login&username=" + encode(userId) + "&password=" + encode(password);
    conn.setRequestProperty("Content-Length", String.valueOf(postData.length()));
    OutputStream wr = conn.getOutputStream();
    wr.write(postData.getBytes());
    wr.close();
    int responseCode = conn.getResponseCode();
    String location = conn.getHeaderField("Location");
    if (responseCode == 302) {
      if (location.indexOf("Main.action") != -1) {
        String cookie = conn.getHeaderField("Set-Cookie");
        String[] fields = cookie.split(";\\s*");
        return fields[0];
      }
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.LOGIN_FAILURE);
    }
    if (responseCode >= 500) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.BACKEND_ERROR);
    }
    throw new MessageHandlerException(MessageHandlerException.ErrorCode.INTERNAL_FAILURE);
  }

  private static void sendMessage(String message, String number, String userId, String cookie) throws IOException, MessageHandlerException {
    URL url = new URL("http://wwwe.way2sms.com/quicksms.action");
    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    setupConnection(conn);
    conn.setRequestProperty("Referer", "http://wwwe.way2sms.com/jsp/InstantSMS.jsp");
    conn.setRequestProperty("Cookie", cookie);
    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    String postData = "HiddenAction=instantsms&Action=" + userId + "&MobNo=" + number + "&textArea=" + encode(message);
    conn.setRequestProperty("Content-Length", String.valueOf(postData.length()));
    OutputStream wr = conn.getOutputStream();
    wr.write(postData.getBytes());
    wr.flush();
    wr.close();
    int responseCode = conn.getResponseCode();
    if (responseCode == 200) {
      return;
    }
    if (responseCode == 302) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.COOKIE_EXPIRED);
    }
    if (responseCode >= 500) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.BACKEND_ERROR);
    }
    throw new MessageHandlerException(MessageHandlerException.ErrorCode.INTERNAL_FAILURE);
  }

  private static String loginAndUpdateCookie(String userName, String password) throws MessageHandlerException
  {
    try {
      String cookie = login(userName, password);
      MAP.put(userName, cookie);
      return cookie; } catch (IOException e) {
    }
    throw new MessageHandlerException(MessageHandlerException.ErrorCode.CONNECTION_ERROR);
  }

  private static boolean matchIdNodeDone(char c)
  {
    if (USER_ID_NODE_MATCHER[(usedIdNodeMatcherCounter + 1)] == c) {
      usedIdNodeMatcherCounter += 1;
      if (usedIdNodeMatcherCounter == USER_ID_NODE_MATCHER.length - 1)
        return true;
    }
    else if (USER_ID_NODE_MATCHER[0] == c) {
      usedIdNodeMatcherCounter = 0;
    } else {
      usedIdNodeMatcherCounter = -1;
    }
    return false;
  }

  private static String extractUserId(char c) {
    if (c == '"') {
      if (extractionStarted) {
        return userIdMatcher.toString();
      }
      extractionStarted = true;
      return null;
    }

    if (extractionStarted) {
      userIdMatcher.append(c);
    }
    return null;
  }

  public static String getUserId(String userName, String password) throws MessageHandlerException {
    try {
      String cookie = loginAndUpdateCookie(userName, password);
      usedIdNodeMatcherCounter = -1;
      userIdMatcher = new StringBuffer();
      extractionStarted = false;
      URL url = new URL("http://wwwe.way2sms.com/jsp/InstantSMS.jsp");
      HttpURLConnection conn = (HttpURLConnection)url.openConnection();
      setupConnection(conn);

      conn.setRequestProperty("Cookie", cookie);
      int responseCode = conn.getResponseCode();
      if (responseCode == 200) {
        InputStream is = conn.getInputStream();

        String userId = null;
        boolean done = false;
        int c;
        while ((c = is.read()) != -1)
        {
          char ch = (char)c;
          if (!done) {
            done = matchIdNodeDone(ch);
          }
          else {
            userId = extractUserId(ch);
            if (userId != null) {
              is.close();
              return userId;
            }
          }
        }
      }
      if (responseCode >= 500)
        throw new MessageHandlerException(MessageHandlerException.ErrorCode.BACKEND_ERROR);
    }
    catch (IOException e) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.CONNECTION_ERROR);
    } catch (MessageHandlerException e) {
      throw e;
    } catch (Exception e) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.INTERNAL_FAILURE);
    }
    throw new MessageHandlerException(MessageHandlerException.ErrorCode.INTERNAL_FAILURE);
  }

  public static void send(String userName, String password, String userId, String number, String message) throws MessageHandlerException
  {
    String cookie = (String)MAP.get(userName);
    boolean loginTriedOnce = false;
    if (cookie == null) {
      cookie = loginAndUpdateCookie(userName, password);
      loginTriedOnce = true;
    }
    try {
      sendMessage(message, number, userId, cookie);
    } catch (IOException e) {
      throw new MessageHandlerException(MessageHandlerException.ErrorCode.CONNECTION_ERROR);
    } catch (MessageHandlerException e) {
      if (e.code == MessageHandlerException.ErrorCode.COOKIE_EXPIRED) {
        if (loginTriedOnce) {
          throw new MessageHandlerException(MessageHandlerException.ErrorCode.INTERNAL_FAILURE);
        }
        cookie = loginAndUpdateCookie(userName, password);
        try {
          sendMessage(message, number, userId, cookie);
        } catch (IOException e1) {
          throw new MessageHandlerException(MessageHandlerException.ErrorCode.CONNECTION_ERROR);
        } catch (MessageHandlerException e1) {
          if (e1.code == MessageHandlerException.ErrorCode.COOKIE_EXPIRED) {
            e1.code = MessageHandlerException.ErrorCode.INTERNAL_FAILURE;
          }
          throw e1;
        }
      }
    }
  }
}