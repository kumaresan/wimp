package wimp.notification.email;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// sendMail("ushashree89@gmail.com,swetha.musicmaniac@gmail.com");

	}

	public static void sendMail(String mailIds, String text) {

		// Get system properties
		Properties p = System.getProperties();
		try {

			// Get session
			p.put("mail.smtp.host",
					System.getProperty("smtpserver", "atom.corp.ebay.com"));
			Session session = Session.getDefaultInstance(p, null);
			System.out.println("session!!");
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ushb@ebay.com"));

			// build the list of recipients
			String mailId[] = mailIds.split(",");
			InternetAddress[] addressTo = new InternetAddress[mailId.length];
			for (int i = 0; i < mailId.length; i++) {
				addressTo[i] = new InternetAddress(mailId[i]);
			}

			message.setRecipients(Message.RecipientType.TO, addressTo);
			message.setSubject("Shipment Tracker");
			message.setText(text);

			// Send message
			Transport.send(message);

		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
