package wimp;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import models.EBayAddress;
import models.EBayTransaction;
import models.EBayUser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import play.libs.XPath;
import wimp.api.EbayXmlApi;
import wimp.api.GoogleApi;
import wimp.api.PitneyApi;
import wimp.notification.email.MailClient;
import wimp.notification.sms.Way2SmsHelper;
import wimp.viewobject.Address;
import wimp.viewobject.Buyer;
import wimp.viewobject.Transaction;

public class WimpManager {

	public static EBayUser addUser(String userid, String authtoken) {
		EBayUser eBayUser = null;
		try {
			eBayUser = getUser(userid);
			if (authtoken != null) {
				eBayUser.authToken = authtoken;
			}
		} catch (Exception e) {
			eBayUser = new EBayUser(userid, authtoken, null, null);
		}
		eBayUser.save();
		return eBayUser;
	}

	public static EBayUser getUser(String userid) throws Exception {
		EBayUser eBayUser = EBayUser.find("byUserid", userid).first();
		if (eBayUser == null) {
			throw new Exception("User Not Found " + userid);
		}
		return eBayUser;
	}

	public static void addPhoneToUser(String userid, String phoneno)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		eBayUser.phoneno = phoneno;
		eBayUser.save();
	}

	public static void addEmailToUser(String userid, String email)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		eBayUser.email = email;
		eBayUser.save();
	}

	public static EBayTransaction addTransaction(String transactionid,
			String itemid, String title, String url, String imageUrl,
			EBayUser seller, EBayUser buyer, String trackingid, String carrier,
			String sellerAddress, String sellerLat, String sellerLng,
			String buyerAddress, String buyerLat, String buyerLng,
			Boolean delivered) throws Exception {
		EBayTransaction eBayTransaction = EBayTransaction.find(
				"byTransactionid", transactionid).first();
		if (eBayTransaction == null) {
			eBayTransaction = new EBayTransaction(transactionid, itemid, title,
					url, imageUrl, seller, buyer, trackingid, carrier,
					sellerAddress, sellerLat, sellerLng, buyerAddress,
					buyerLat, buyerLng, delivered);
			eBayTransaction.save();
		}
		return eBayTransaction;
	}

	public static List<EBayTransaction> getBuyerTrasactions(String userid)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		List<EBayTransaction> transactions = EBayTransaction.find("byBuyer",
				eBayUser).fetch();
		return transactions;
	}

	public static List<EBayAddress> getTrasactionsAddresses(
			EBayTransaction eBayTransaction) throws Exception {
		// TODO Order by
		List<EBayAddress> addressses = EBayAddress.find("byTransaction",
				eBayTransaction).fetch();
		System.out.println("WimpManager.getTrasactionsAddresses() "
				+ addressses.size());
		return addressses;
	}

	public static List<EBayTransaction> getSellerTransactions(String userid)
			throws Exception {
		EBayUser eBayUser = getUser(userid);
		List<EBayTransaction> transactions = EBayTransaction.find("bySeller",
				eBayUser).fetch();
		return transactions;
	}

	private static EBayUser addBuyerFromTransactionXml(Element transaction)
			throws Exception {
		String buyeruserid = XPath.selectText("Buyer/UserID", transaction);
		System.out
				.println("WimpManager.storeUser() buyeruserid " + buyeruserid);

		String buyeremail = XPath.selectText("Buyer/Email", transaction);
		System.out.println("WimpManager.storeUser() buyeremail " + buyeremail);
		EBayUser buyer = addUser(buyeruserid, null);
		addEmailToUser(buyeruserid, buyeremail);

		return buyer;
	}

	public static String getBuyerAddressFromTransactionXml(Element transaction)
			throws Exception {
		NodeList nodelist = transaction.getElementsByTagName("ShippingAddress");
		Element ShippingAddress = (Element) nodelist.item(0);
		String Street1 = XPath.selectText("Street1", ShippingAddress);
		String CityName = XPath.selectText("CityName", ShippingAddress);
		String StateOrProvince = XPath.selectText("StateOrProvince",
				ShippingAddress);
		String Country = XPath.selectText("Country", ShippingAddress);
		String PostalCode = XPath.selectText("PostalCode", ShippingAddress);
		String address = Street1 + ", " + CityName + ", " + StateOrProvince
				+ ", " + Country + ", " + PostalCode;
		System.out.println("WimpManager.getBuyerAddress() " + address);
		return address;
	}

	public static void storeUser(String userid, String authToken)
			throws Exception {

		String sellerTransactionsXmlResponse = EbayXmlApi
				.getSellerTransactions(authToken);

		// TODO save auth token also
		EBayUser seller = addUser(userid, null);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document xml = db.parse(new ByteArrayInputStream(
				sellerTransactionsXmlResponse.getBytes()));
		System.out.println("WimpManager.storeUser() xml " + xml);

		// for (Node transaction : XPath.selectNodes("Transaction", xml)) {

		System.out.println("WimpManager.storeUser() " + xml);
		NodeList nodelist = xml.getElementsByTagName("Transaction");
		for (int i = 0; i < nodelist.getLength(); i++) {
			System.out.println("WimpManager.storeUser() " + nodelist.item(i));

			Element transaction = (Element) nodelist.item(i);
			System.out.println("WimpManager.storeUser() transaction "
					+ transaction);

			String transactionid = XPath.selectText("TransactionID",
					transaction);
			System.out.println("WimpManager.storeUser() transactionid "
					+ transactionid);

			EBayUser buyer = addBuyerFromTransactionXml(transaction);
			String itemid = XPath.selectText("Item/ItemID", transaction);
			String[] temp = EbayXmlApi.getItem(authToken, itemid);

			String buyerAddress = getBuyerAddressFromTransactionXml(transaction);

			EBayTransaction eBayTransaction = addTransaction(transactionid,
					itemid, temp[1], temp[2], temp[3], seller, buyer, null,
					null, temp[4], null, null, buyerAddress, null, null, false);
			checkShipmentUpdates(eBayTransaction);
			geoCodeTransaction(eBayTransaction);
		}

	}

	public static Buyer getBuyer(String userid) throws Exception {
		Buyer buyer = new Buyer();

		EBayUser user = getUser(userid);
		buyer.userid = user.userid;
		buyer.email = user.email;
		buyer.phoneno = user.phoneno;
		buyer.transactions = new ArrayList<Transaction>();
		List<EBayTransaction> eBayTransactions = getBuyerTrasactions(userid);
		fillTransactions(buyer.transactions, eBayTransactions);

		buyer.soldtransactions = new ArrayList<Transaction>();
		List<EBayTransaction> eBaySoldTransactions = getSellerTransactions(userid);
		fillTransactions(buyer.soldtransactions, eBaySoldTransactions);

		return buyer;
	}

	private static void fillTransactions(List<Transaction> transactions,
			List<EBayTransaction> eBayTransactions) throws Exception {
		for (EBayTransaction eBayTransaction : eBayTransactions) {
			Transaction transaction = new Transaction();
			transaction.itemid = eBayTransaction.itemid;
			transaction.title = eBayTransaction.title;
			transaction.url = eBayTransaction.url;
			transaction.imageUrl = eBayTransaction.imageUrl;

			if (eBayTransaction.seller != null) {
				transaction.sellerName = eBayTransaction.seller.userid;
			}

			transaction.trackingid = eBayTransaction.trackingid;
			transaction.carrier = eBayTransaction.carrier;

			transaction.sellerAddress = eBayTransaction.sellerAddress;
			transaction.sellerLat = eBayTransaction.sellerLat;
			transaction.sellerLng = eBayTransaction.sellerLng;
			transaction.buyerAddress = eBayTransaction.buyerAddress;
			transaction.buyerLat = eBayTransaction.buyerLat;
			transaction.buyerLng = eBayTransaction.buyerLng;
			transaction.delivered = eBayTransaction.delivered;

			transaction.addresses = new ArrayList<Address>();
			List<EBayAddress> eBayAddresses = getTrasactionsAddresses(eBayTransaction);
			System.out.println("WimpManager.fillTransactions() "
					+ eBayAddresses.size());
			for (EBayAddress eBayAddress : eBayAddresses) {
				Address address = new Address();
				address.address = eBayAddress.address;
				address.lat = eBayAddress.lat;
				address.lng = eBayAddress.lng;
				address.scandiscription = eBayAddress.scandiscription;

				transaction.addresses.add(address);
			}
			transactions.add(transaction);
		}

	}

	public static void checkShipmentUpdates(EBayTransaction eBayTransaction)
			throws Exception {
		
		// do not process pitnybose. 
		if (true || eBayTransaction.trackingid == null
				|| "".equals(eBayTransaction.trackingid)) {
			return;
		}

		List<EBayAddress> trackingAddresses = PitneyApi.trackerDetails(
				eBayTransaction.trackingid, eBayTransaction.carrier);

		System.out.println("WimpManager.processTransaction() Pitny "
				+ trackingAddresses.size());

		List<EBayAddress> storedAddress = EBayAddress.find("byTransaction",
				eBayTransaction).fetch();

		for (EBayAddress eBayAddress : storedAddress) {
			eBayAddress.delete();
		}

		// save address from pitny
		int j = 1;
		for (int i = trackingAddresses.size() - 1; i > 0; i--) {
			EBayAddress eBayAddress = trackingAddresses.get(i - 1);
			eBayAddress.sequence = j;
			eBayAddress.transaction = eBayTransaction;
			System.out.println("WimpManager.storeNewTrackingDetails() save 2 "
					+ eBayAddress.address + " " + eBayAddress.sequence);
			eBayAddress.save();
			j++;
		}
	}

	public static void geoCodeTransaction(EBayTransaction eBayTransaction)
			throws Exception {
		String[] tmpLatLng = null;

		if (eBayTransaction.buyerLat == null
				|| "".equals(eBayTransaction.buyerLat)) {
			if (eBayTransaction.buyerAddress != null) {
				tmpLatLng = GoogleApi.geoCode(eBayTransaction.buyerAddress);
				eBayTransaction.buyerLat = tmpLatLng[0];
				eBayTransaction.buyerLng = tmpLatLng[1];
			}

			if (eBayTransaction.sellerAddress != null) {
				tmpLatLng = GoogleApi.geoCode(eBayTransaction.sellerAddress);
				eBayTransaction.sellerLat = tmpLatLng[0];
				eBayTransaction.sellerLng = tmpLatLng[1];
			}

			eBayTransaction.save();
		}

		List<EBayAddress> eBayAddresses = getTrasactionsAddresses(eBayTransaction);

		if (eBayAddresses == null) {
			return;
		}

		for (EBayAddress eBayAddress : eBayAddresses) {
			if (eBayAddress.lat == null || "".equals(eBayAddress.lat.trim())
					|| eBayAddress.lng == null
					|| "".equals(eBayAddress.lng.trim())) {

				String[] latLng = GoogleApi.geoCode(eBayAddress.address);
				eBayAddress.lat = latLng[0];
				eBayAddress.lng = latLng[1];

				System.out.println("WimpManager.geoCodeTransaction() save 4 "
						+ eBayAddress.address + " " + eBayAddress.sequence
						+ " " + eBayAddress.lat + " " + eBayAddress.lng);

				eBayAddress.save();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("WimpManager.main()");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document xml = db.parse("formatting.xml");
		System.out.println("WimpManager.main() " + xml);
		NodeList nodelist = xml.getElementsByTagName("Transaction");

		for (int i = 0; i < nodelist.getLength(); i++) {
			System.out.println("WimpManager.main() " + nodelist.item(i));
			Element transaction = (Element) nodelist.item(i);
			// for (Node transaction : XPath.selectNodes("Transaction", xml)) {
			String transactionid = transaction
					.getElementsByTagName("TransactionID").item(0)
					.getFirstChild().getTextContent();
			System.out.println("WimpManager.main() " + transactionid);
			String buyeruserid = transaction
					.getElementsByTagName("TransactionID").item(0)
					.getFirstChild().getTextContent();
			System.out.println("WimpManager.storeUser() buyeruserid "
					+ buyeruserid);
			String buyeremail = XPath.selectText("Buyer/Email", transaction);
			System.out.println("WimpManager.storeUser() buyeremail "
					+ buyeremail);
			String itemid = XPath.selectText("Item/ItemID", transaction);
			System.out.println("WimpManager.storeUser() itemid " + itemid);
		}

	}

	public static void sendSMS(String phoneno, String message) {
		Way2SmsHelper.sendMessage(phoneno, message);
	}

	public static void sendEmail(String email, String message) {
		MailClient mail = new MailClient();
		mail.sendMail(email, message);
	}

}
