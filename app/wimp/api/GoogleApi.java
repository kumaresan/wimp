package wimp.api;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GoogleApi {
	public static String[] geoCode(String address) throws Exception {
		URL url = null;
		String addressDetails = address.replace(" ", "%20");
		// System.out.println("address details " + addressDetails);
		String URLaddress = "http://maps.googleapis.com/maps/api/geocode/xml?address="
				+ addressDetails + "&sensor=false";

		url = new URL(URLaddress);
		System.out.println(URLaddress);

		URLConnection connection = null;
		connection = url.openConnection();
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setRequestProperty("accept-charset", "UTF-8");
		connection.setRequestProperty("content-type",
				"application/x-www-form-urlencoded");
		BufferedReader inBuff = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));

		String decodedString;
		String response = "";

		while ((decodedString = inBuff.readLine()) != null) {
			response += decodedString;
		}

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document scanDetails = db.parse(new ByteArrayInputStream(response
				.getBytes()));

		NodeList scanData = scanDetails.getElementsByTagName("location");

		String[] ret = new String[2];
		for (int temp = 0; temp < scanData.getLength(); temp++) {

			Node nNode = scanData.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				ret[0] = getTagValue("lat", eElement);
				ret[1] = getTagValue("lng", eElement);
			}
		}
		return ret;
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);
		if (nValue != null)
			return nValue.getNodeValue();
		else
			return "";
	}

}
