package wimp.viewobject;

import java.util.List;

public class Transaction {
	public String itemid;
	public String title;
	public String url;
	public String imageUrl;
	public String sellerName;
	public String trackingid;
	public String carrier;
	public String sellerAddress;
	public String sellerLat;
	public String sellerLng;
	public String buyerAddress;
	public String buyerLat;
	public String buyerLng;
	public Boolean delivered;
	public List<Address> addresses;
}
