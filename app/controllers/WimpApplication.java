package controllers;

import java.util.List;

import models.EBayAddress;
import models.EBayTransaction;
import models.EBayUser;

import play.mvc.Controller;
import wimp.WimpManager;
import wimp.api.EbayXmlApi;
import wimp.api.GoogleApi;
import wimp.viewobject.Buyer;

public class WimpApplication extends Controller {

	public static String SESSION_EBAY_SESSIONID = "ebaySessionId";
	public static String SESSION_EBAY_USERID = "ebayUserId";
	public static String SESSION_EBAY_AUTH_TOKEN = "ebayAuthToken";

	public static void index() {
		System.out.println("WimpApplication.index()");
		render();
	}

	public static void login() {
		System.out.println("Application.login()");

		try {
			String sessionID = EbayXmlApi.getSessionID();
			session.put(SESSION_EBAY_SESSIONID, sessionID);
			String signInUrl = EbayXmlApi.getSignInUrl(sessionID);
			redirect(signInUrl);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}

	}

	public static void logincomplete() {
		System.out.println("Application.logincomplete()");
		String sessionID = session.get(SESSION_EBAY_SESSIONID);
		System.out
				.println("Application.logincomplete() sessionID " + sessionID);

		try {
			String authToken = EbayXmlApi.fetchToken(sessionID);
			session.put(SESSION_EBAY_AUTH_TOKEN, authToken);
			System.out.println("Application.logincomplete() authToken "
					+ authToken);
			String userID = EbayXmlApi.getUserID(authToken);
			session.put(SESSION_EBAY_USERID, userID);
			System.out.println("Application.logincomplete() userID " + userID);

			WimpManager.storeUser(userID, authToken);

			buyer(userID);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}

	}

	public static void subscribephone(String userid, String phoneno) {

		try {
			EBayUser user = (EBayUser) EBayUser.find("byUserid", userid)
					.first();
			user.phoneno = phoneno;
			user.save();
			WimpManager.sendSMS(phoneno,
					"You have been subscribed to Wimp SMS Alerts");

			flash.success("You have been subscribed to Wimp SMS Alerts on  "
					+ phoneno, "");

			buyer(userid);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void unsubscribephone(String userid) {

		try {
			EBayUser user = (EBayUser) EBayUser.find("byUserid", userid)
					.first();
			user.phoneno = null;
			user.save();
			flash.success("You have been unsubscribed to Wimp SMS Alerts");
			buyer(userid);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void subscribeemail(String userid, String email) {

		try {
			EBayUser user = (EBayUser) EBayUser.find("byUserid", userid)
					.first();
			user.email = email;
			user.save();
			WimpManager.sendEmail(email,
					"You have been subscribed to Wimp Email Alerts");
			flash.success("You have been subscribed to Wimp Email Alerts on  "
					+ email, "");
			buyer(userid);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void unsubscribeemail(String userid) {

		try {
			EBayUser user = (EBayUser) EBayUser.find("byUserid", userid)
					.first();
			user.email = null;
			user.save();
			flash.success("You have been unsubscribed from Wimp Email Alerts");
			buyer(userid);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void logout() {
		System.out.println("WimpApplication.logout()");
		session.remove(SESSION_EBAY_AUTH_TOKEN);
		session.remove(SESSION_EBAY_USERID);
		session.remove(SESSION_EBAY_SESSIONID);
		index();
	}

	public static void buyer(String userid) {
		try {
			Buyer buyer = WimpManager.getBuyer(userid);
			render(buyer);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}
	}

	public static void buyerjson(String userid) {
		try {
			Buyer buyer = WimpManager.getBuyer(userid);
			renderJSON(buyer);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}
	}

	public static void processtransaction() {
		try {
			List<EBayTransaction> eBayTransactions = EBayTransaction.all()
					.fetch();
			for (EBayTransaction eBayTransaction : eBayTransactions) {
				WimpManager.checkShipmentUpdates(eBayTransaction);
				WimpManager.geoCodeTransaction(eBayTransaction);
			}
			renderJSON(eBayTransactions);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}
	}

	public static void cleanup() {
		EBayAddress.deleteAll();
		EBayTransaction.deleteAll();
		EBayUser.deleteAll();
		redirect("/admin");
	}

	public static void backend() {
		render();
	}

	public static void addaddress(String transactionid, String address,
			String status) {

		try {
			EBayTransaction eBayTransaction = EBayTransaction.find(
					"byTransactionid", transactionid).first();
			List<EBayAddress> addresses = EBayAddress.find("byTransaction",
					eBayTransaction).fetch();

			int max = 0;
			if (addresses != null) {
				for (EBayAddress eBayAddress : addresses) {
					if (eBayAddress.sequence > max) {
						max = eBayAddress.sequence;
					}
				}
			}

			EBayAddress newAddress = new EBayAddress();
			newAddress.address = address;
			newAddress.scandiscription = status;
			newAddress.sequence = max + 1;
			newAddress.transaction = eBayTransaction;

			String[] temp;

			temp = GoogleApi.geoCode(address);
			newAddress.lat = temp[0];
			newAddress.lng = temp[1];

			newAddress.save();

			EBayUser user = eBayTransaction.buyer;
			String message = "Hi " + user.userid + ",\n" + "Your item : "
					+ eBayTransaction.title + " has reached " + address
					+ ".\nStatus is: " + status;

			if (user.email != null) {
				WimpManager.sendEmail(user.email, message);
			}

			if (user.phoneno != null) {
				WimpManager.sendSMS(user.phoneno, message);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		backend();
	}

}
