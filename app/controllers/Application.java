package controllers;

import java.util.ArrayList;
import java.util.List;

import play.mvc.Controller;
import wimp.api.EbayXmlApi;

public class Application extends Controller {

	public static String SESSION_EBAY_SESSIONID = "ebaySessionId";
	public static String SESSION_EBAY_USERID = "ebayUserId";
	public static String SESSION_EBAY_AUTH_TOKEN = "ebayAuthToken";

	public static void index() {
		render();
	}

	public static void login() {
		System.out.println("Application.login()");

		try {
			String sessionID = EbayXmlApi.getSessionID();
			session.put(SESSION_EBAY_SESSIONID, sessionID);
			String signInUrl = EbayXmlApi.getSignInUrl(sessionID);
			redirect(signInUrl);
		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}

	}

	public static void logincomplete() {
		System.out.println("Application.logincomplete()");
		String sessionID = session.get(SESSION_EBAY_SESSIONID);
		System.out
				.println("Application.logincomplete() sessionID " + sessionID);

		try {
			String authToken = EbayXmlApi.fetchToken(sessionID);
			session.put(SESSION_EBAY_AUTH_TOKEN, authToken);
			System.out.println("Application.logincomplete() authToken "
					+ authToken);
			String userID = EbayXmlApi.getUserID(authToken);
			session.put(SESSION_EBAY_USERID, userID);
			System.out.println("Application.logincomplete() userID " + userID);
			items();

		} catch (Exception e) {
			e.printStackTrace();
			error(e);
		}

	}

	public static void items() throws Exception {
		String userid = session.get(SESSION_EBAY_USERID);
		String authToken = session.get(SESSION_EBAY_AUTH_TOKEN);

//		List<String> items = EbayXmlApi.getSellerTransactions(authToken);

//		render(userid, items);
	}

	public static void item(String item) {
		String username = "username";
		render(username, item);
	}

}